package txhostsim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Harro Lissenberg
 */
public class Server {

    private ServerSocket socket;
    private Socket client;

    public static void main(String[] args) throws IOException {
        int port = 5557;
        if (args.length == 1) {
            port = Integer.parseInt(args[0]);
        }
        while (true) {
            // continuously accept connections
            new Server(port).handleSingleConnection(new TxHostHandler());
        }

    }

    public Server(int port) throws IOException {
        socket = new ServerSocket(port);
    }

    /**
     * As the name implies this method only handles a single connection at a time.
     *
     * @throws IOException
     */
    public void handleSingleConnection(MessageHandler handler) throws IOException {
        client = socket.accept();
        BufferedReader in =
                new BufferedReader(new InputStreamReader(client.getInputStream()));
        System.out.println("Client connected: " + client.getInetAddress().getHostAddress());

        while (true) {
            StringBuffer req = new StringBuffer();
            int c;
            while ((c = in.read()) != -1) {
                // null byte ends message
                if (c == 0x00) {
                    break;
                }
                req.append((char) c);
            }
            if (req.length() > 0) {
                handler.handle(req.toString(), client.getOutputStream());
            }

        }

    }

}

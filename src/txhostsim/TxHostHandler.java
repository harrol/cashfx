package txhostsim;

import cashregister.Messages;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Harro Lissenberg
 */
public class TxHostHandler implements MessageHandler {
    @Override public void handle(String message, OutputStream out) throws IOException {
        System.out.println(now() + ": " + message);
        if (message != null && message.length() > 5) {
            String tag = message.substring(0, 5);
            switch (tag) {
                case "<Intr":
                    introductionRequest(message, out);
                    break;
                case "<Card":
                    cardRequest(message, out);
                    break;
                case "<Paym":
                    paymentRequest(message, out);
                    break;
                default:
                    // ignore
            }
        }
    }

    private void introductionRequest(String message, OutputStream out) throws IOException {
        send(Messages.INTRODUCTION_RESPONSE_OK, out);
    }

    private void cardRequest(String message, OutputStream out) throws IOException {
        // TODO handle via GUI
        send("CARD ACK", out);
        send("CARD PRES", out);
        send("CARD AUT", out);
    }

    private void paymentRequest(String message, OutputStream out) throws IOException {
        // TODO handle via GUI
        send("PAY ACK", out);
        send("PAY OK", out);
    }


    private void send(String message, OutputStream out) throws IOException {
        out.write(message.getBytes("UTF-8"));
        out.write(0x00);
        out.flush();
    }

    public static String now() {
        return new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
    }
}

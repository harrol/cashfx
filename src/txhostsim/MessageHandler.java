package txhostsim;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Harro Lissenberg
 */
public interface MessageHandler {

    /**
     * Handle the incoming message
     *
     * @param message incoming
     * @return response message
     */
    public void handle(String message, OutputStream out) throws IOException;
}

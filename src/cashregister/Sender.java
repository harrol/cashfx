package cashregister;

import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Harro Lissenberg
 */
public class Sender {

    private Socket socket;
    private BufferedWriter writer;
    private BufferedReader reader;
    private TextArea request;
	private Handler handler;


    public Sender(TextArea request, TextArea response) {
        this.request = request;
        request.clear();
        response.clear();
		handler = new Handler(response);
    }

    /**
     * Creates a connection with the TxHost and sends an introduction request
     *
     * @return TxHost response
     */
    public void connectAndRegister() throws IOException {
        socket = new Socket(Config.HOST_IP, Config.HOST_PORT);
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        send(Messages.getIntroductionRequest(Config.CASH_REGISTER_ID), 1);
    }

    public void keepAlive() throws IOException {
        send(Messages.KEEP_ALIVE_REQUEST, 1);
    }

    public void requestCard() throws IOException {
        send(Messages.CARD_REQUEST, 3);
    }

    public void pay(String amount, String message) throws IOException {
        send(Messages.getPaymentRequest(amount, message), 2);
    }

	public void cancel() throws IOException {
		send(Messages.getCancelRequest(""), 0);
	}


    public static String now() {
        return new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
    }

    private void send(String message, int messageCount) throws IOException {
        request.appendText(now() + ": " + message + "\n");
        writer.write(message);
        writer.write('\000'); // null byte to end stream
        writer.flush();
        for (int i = 0; i < messageCount; i++) {
            int c;
            StringBuffer res = new StringBuffer();
            while ((c = reader.read()) > 0) {
                // null byte ends message
                res.append((char) c);
            }
            String responseMessage = res.toString();
			handler.handle(responseMessage);
        }
    }

    public void disconnect() {
        try {
            if (socket != null) {
                reader.close();
                writer.close();
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

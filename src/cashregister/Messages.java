package cashregister;

import java.lang.String; /**
 * @author Harro Lissenberg
 */
public class Messages {
    public static final String INTRODUCTION_REQUEST = "<IntroductionRequest><cashRegisterId>$cashRegisterId$</cashRegisterId><cashRegisterType>fixed</cashRegisterType><protocolVersion>v1.0</protocolVersion><locationId></locationId></IntroductionRequest>";
    public static final String INTRODUCTION_RESPONSE_OK = "INTRODUCTION_ACKNOWLEDGED_OK";

    public static final String CARD_REQUEST = "<CardRequest></CardRequest>";
    public static final String CARD_REQUEST_ACKNOWLEDGED = "CARDREQUEST_ACKNOWLEDGED";
    public static final String CARD_PRESENTED = "CARD_PRESENTED";
    public static final String CARD_AUTHENTICATED = "CARD_AUTHENTICATED";

    public static final String PAYMENT_REQUEST = "<PaymentRequest><amount>$amount$</amount><currency>EUR</currency><description>$description$</description></PaymentRequest>";
    public static final String PAYMENT_REQUEST_ACKNOWLEDGED = "PAYMENTREQUEST_ACKNOWLEDGED";
    public static final String PAYMENT_SUCCEEDED = "PAYMENT_SUCCEEDED";

    public static final String KEEP_ALIVE_REQUEST = "<KeepAlive></KeepAlive>";
    public static final String KEEP_ALIVE_RESPONSE = "<KeepAliveAcknowledge/>";

	public static final String CANCEL_REQUEST = "<CancelTransaction><transactionId>$id$<transactionId><CancelTransaction>";

    /**
     * Returns an introduction request
     *
     * @param cashRegisterId the id of this cash register
     * @return
     */
    public static String getIntroductionRequest(String cashRegisterId) {
        return INTRODUCTION_REQUEST.replace("$cashRegisterId$", cashRegisterId);
    }

	public static String getCancelRequest(String transactionId) {
		return CANCEL_REQUEST.replace("$id$", transactionId);
	}

    /**
     * Returns a payment request message
     *
     * @param amount      the amount in cents
     * @param description the description
     * @return
     */
    public static String getPaymentRequest(String amount, String description) {
        return PAYMENT_REQUEST.replace("$amount$", amount).replace("$description$", description);
    }

}

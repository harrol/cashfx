package cashregister;

import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.Blend;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * @author Harro Lissenberg
 */
public class Main extends Application {

    private TextField amount;
    private TextField message;
    private TextArea request;
    private TextArea response;
    private CheckBox wrapOn;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setHgap(10);
        grid.setVgap(5);
        grid.setGridLinesVisible(false);
        wrapOn = new CheckBox("Wrap");
        wrapOn.setIndeterminate(false);
        wrapOn.setSelected(false);
        wrapOn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(wrapOn.isSelected()) {
                    request.setWrapText(true);
                    response.setWrapText(true);
                } else {
                    request.setWrapText(false);
                    response.setWrapText(false);
                }
            }
        });
        request = new TextArea();
        request.setPrefWidth(800);
        request.setPrefHeight(150);
        request.setEffect(new Blend());
        response = new TextArea();
        response.prefWidthProperty().bind(stage.widthProperty());
        response.setPrefHeight(250);
        response.setEffect(new DropShadow());

        final Sender s = new Sender(request, response);
        Button pay = new Button("Pay");
        final Button cancel = new Button("Cancel");
        cancel.setDisable(true);
        cancel.setEffect(new Reflection());
        cancel.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Task task = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        s.cancel();
                        return null;
                    }
                };
                new Thread(task).start();
            }
        });

        pay.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                Task task = new Task<Void>() {

                    @Override
                    protected Void call() throws Exception {
                        cancel.setDisable(false);
                        try {
                            s.connectAndRegister();
                            s.requestCard();
                            s.pay(amount.getText(), message.getText());
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            s.disconnect();
                            cancel.setDisable(true);
                        }
                        return null;
                    }
                };
                new Thread(task).start();
            }
        });
        pay.setEffect(new Reflection());
        Label amountLabel = new Label("Amount");
        amount = new TextField("1");
        amount.setEffect(new InnerShadow());
        amount.setMaxWidth(200);
        amount.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                amount.setText(amount.getText().replaceAll("[^0-9]", ""));
                amount.end();
            }
        });

        Label messageLabel = new Label("Description");
        message = new TextField("Lunch bill");
        message.setEffect(new InnerShadow());
        message.setMaxWidth(200);
        message.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                String s = message.getText();
                if(s.length() > 20) {
                    message.setText(s.substring(0, 20));
                    message.end();
                }
            }
        });
        grid.getChildren().addAll(pay, cancel, amount, message, messageLabel, amountLabel, request, response, wrapOn);
        GridPane.setConstraints(amountLabel, 0, 0);
        GridPane.setConstraints(amount, 1, 0, 3, 1);
        GridPane.setConstraints(messageLabel, 0, 1);
        GridPane.setConstraints(message, 1, 1, 3, 1);
        GridPane.setConstraints(cancel, 0, 2);
        GridPane.setConstraints(pay, 1, 2);
        GridPane.setConstraints(wrapOn, 2, 2);
        GridPane.setConstraints(request, 0, 3, 3, 1);
        GridPane.setConstraints(response, 0, 4, 3, 1);
        stage.setTitle("CashFX");
        Scene scene = new Scene(grid, Color.LIGHTGREY);
        stage.setScene(scene);
        stage.show();
    }
}

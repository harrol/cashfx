package cashregister;

import javafx.scene.control.TextArea;

/**
 * User: hlis
 * Date: 26/09/12
 */
public class Handler {

	enum State {IDLE, INTRO, CONNECTED, CARDREQUESTED, CARDREQUESTACK, CARDPRESENTED, PAYMENTREQUESTED, PAYMENTRESPONDED}

	private State state = State.IDLE;
	private TextArea textArea;

	public Handler(TextArea textArea) {
		this.textArea = textArea;
	}

	public void handle(String message) {
		textArea.appendText(Sender.now() + ": " + message + "\n");
		switch (state) {

			case IDLE:
				break;
			case INTRO:
				break;
			case CONNECTED:
				break;
			case CARDREQUESTED:
				break;
			case CARDREQUESTACK:
				break;
			case CARDPRESENTED:
				break;
			case PAYMENTREQUESTED:
				break;
			case PAYMENTRESPONDED:
				break;
		}

	}

}
